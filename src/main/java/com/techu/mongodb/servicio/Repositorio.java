package com.techu.mongodb.servicio;

import com.techu.mongodb.modelo.ModeloProducto;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface Repositorio extends MongoRepository<ModeloProducto, String> {
}
