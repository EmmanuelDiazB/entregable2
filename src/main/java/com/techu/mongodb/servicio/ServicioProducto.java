package com.techu.mongodb.servicio;

import com.techu.mongodb.modelo.ModeloProducto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import java.util.List;
import java.util.Optional;

@Service
public class ServicioProducto {

    @Autowired
    Repositorio productoRepositorio;

    public List<ModeloProducto> findAll() {
        return productoRepositorio.findAll();
    }

    public Optional<ModeloProducto> findById(String id) {
        return productoRepositorio.findById(id);
    }

    public ModeloProducto save(ModeloProducto entidad) {
        return productoRepositorio.save(entidad);
    }

    public boolean delete(ModeloProducto entidad) {
        try {
            productoRepositorio.delete(entidad);
            return true;
        }catch (Exception ex) {
            return false;
        }
    }
}
