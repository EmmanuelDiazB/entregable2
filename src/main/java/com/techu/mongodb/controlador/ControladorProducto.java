package com.techu.mongodb.controlador;

import com.techu.mongodb.modelo.ModeloProducto;
import com.techu.mongodb.servicio.ServicioProducto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2")
public class ControladorProducto {

    @Autowired
    ServicioProducto servicioProducto;

    @GetMapping("/productos")
    public List<ModeloProducto> getProductos() {
        return servicioProducto.findAll();
    }

    @GetMapping("/productos/{id}")
    public Optional<ModeloProducto> getProductoById(@PathVariable String id) {
        return servicioProducto.findById(id);
    }

    @PostMapping("/productos")
    public ModeloProducto postProductos(@RequestBody ModeloProducto nuevoProducto) {
        return servicioProducto.save(nuevoProducto);
    }

    @PutMapping("/productos")
    public void putProductos(@RequestBody ModeloProducto productoActualizar) {
        servicioProducto.save(productoActualizar);
    }

    @DeleteMapping("/productos")
    public boolean deleteProductos(@RequestBody ModeloProducto productoBorrar) {
        return servicioProducto.delete(productoBorrar);
    }
}
